﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _promoCodesRepository;

        public PromocodesController(IRepository<PromoCode> promoCodesRepository)
        {
            _promoCodesRepository = promoCodesRepository;
        }
        
        /// <summary>
        /// Получить все промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync()
        {
            var preferences = await _promoCodesRepository.GetAllAsync();

            var response = preferences.Select(x => new PromoCodeShortResponse()
            {
                Id = x.Id,
                Code = x.Code,
                BeginDate = x.BeginDate.ToString("yyyy-MM-dd"),
                EndDate = x.EndDate.ToString("yyyy-MM-dd"),
                PartnerName = x.PartnerName,
                ServiceInfo = x.ServiceInfo
            }).ToList();

            return Ok(response);
        }

        /*        
        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request)
        {
            var customers = await _customerRepository.GetAllAsync();
            customers = customers.
                Where(x => x.Preferences != null && x.Preferences.
                Any(p => p.Name == request.Preference));
            if (!customers.Any())
            {
                return NotFound();
            }
            var preference = customers.First().Preferences.FirstOrDefault(x => x.Name == request.Preference);

            foreach (var customer in customers)
            {
                var promoCode = new PromoCode
                {
                    ServiceInfo = request.ServiceInfo,
                    Preference = preference,
                    BeginDate = DateTime.Now,
                    Code = request.PromoCode,
                    EndDate = DateTime.Now.AddHours(48), // TODO считывать из конфига или бд время жизни промокода
                    PartnerName = request.PartnerName,
                };
                promoCode = await _promoCodeRepository.AddAsync(promoCode);

                var customerPromoCodes = customer.PromoCodes.ToList();
                if (customerPromoCodes == null)
                {
                    customer.PromoCodes = new List<PromoCode>();
                }
                customerPromoCodes.Add(promoCode);
                customer.PromoCodes = customerPromoCodes;
                await _customerRepository.UpdateAsync(customer);
            }

            return Ok();
        }
        */
    }
}