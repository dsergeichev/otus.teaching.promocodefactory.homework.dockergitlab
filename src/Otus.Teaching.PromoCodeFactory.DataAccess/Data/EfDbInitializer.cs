﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class EfDbInitializer
        : IDbInitializer
    {
        private readonly DefaultDbContext _db;

        public EfDbInitializer(DefaultDbContext dataContext)
        {
            _db = dataContext;
        }
        
        public void InitializeDb()
        {
            if (_db.Database.GetPendingMigrations().Any())
            {
                _db.Database.Migrate();

                try
                {
                    _db.AddRange(FakeDataFactory.Employees);
                    _db.SaveChanges();

                    _db.AddRange(FakeDataFactory.Preferences);
                    _db.SaveChanges();

                    _db.AddRange(FakeDataFactory.Customers);
                    _db.SaveChanges();
                }
                catch (Exception) { }                
            }            
        }
    }
}