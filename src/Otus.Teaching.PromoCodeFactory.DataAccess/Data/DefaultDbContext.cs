﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;


namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class DefaultDbContext : DbContext
    {
        public DbSet<Employee> Employees { get; set; }
        public DbSet<Role> Roles { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<PromoCode> PromoCodes { get; set; }
        public DbSet<Preference> Preferences { get; set; }

        public DefaultDbContext() { }
        public DefaultDbContext(DbContextOptions<DefaultDbContext> options)
            : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id).HasMaxLength(36);
                entity.HasIndex(e => e.Email).IsUnique();
                entity.Property(e => e.Email).IsRequired(true).HasMaxLength(50);
                entity.Property(e => e.FirstName).IsRequired(true).HasMaxLength(50);
                entity.Property(e => e.LastName).IsRequired(true).HasMaxLength(50);
                entity.Navigation(e => e.Role).AutoInclude();
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Id).HasMaxLength(36);
                entity.Property(e => e.Name).IsRequired(true).HasMaxLength(50);
                entity.HasMany<Employee>()
                    .WithOne(e => e.Role);
            });

            modelBuilder.Entity<Customer>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.HasIndex(e => e.Email).IsUnique();
                entity.Property(e => e.Email).IsRequired(true).HasMaxLength(50);
                entity.Property(e => e.FirstName).IsRequired(true).HasMaxLength(50);
                entity.Property(e => e.LastName).IsRequired(true).HasMaxLength(50);

                entity.Navigation(e => e.Preferences).AutoInclude();
                entity.Navigation(e => e.PromoCodes).AutoInclude();

                /*entity.HasMany(p => p.Preferences)
                    .WithMany(p => p.Customers)
                    .UsingEntity<CustomerPreference>(
                        p => p
                            .HasOne(e => e.Preference)
                            .WithMany()
                            .HasForeignKey(pt => pt.PreferenceId),
                        c => c
                            .HasOne(pt => pt.Customer)
                            .WithMany()
                            .HasForeignKey(pt => pt.CustomerId),
                        cp =>
                        {
                            cp.HasKey(t => new { t.CustomerId, t.PreferenceId });
                        });*/

                entity.HasMany(e => e.PromoCodes)
                    .WithOne();
            });

            modelBuilder.Entity<Preference>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Name).IsRequired(true).HasMaxLength(50);
            });

            modelBuilder.Entity<CustomerPreference>()
                .HasKey(bc => new { bc.CustomerId, bc.PreferenceId });
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(bc => bc.Customer)
                .WithMany(b => b.Preferences)
                .HasForeignKey(bc => bc.CustomerId);
            modelBuilder.Entity<CustomerPreference>()
                .HasOne(bc => bc.Preference)
                .WithMany()
                .HasForeignKey(bc => bc.PreferenceId);

            modelBuilder.Entity<PromoCode>(entity =>
            {
                entity.HasKey(e => e.Id);
                entity.Property(e => e.Code).HasMaxLength(50);
                entity.Property(e => e.ServiceInfo).HasMaxLength(50);
                entity.Property(e => e.PartnerName).HasMaxLength(50);

                entity.Navigation(e => e.Preference).AutoInclude();
                entity.Navigation(e => e.PartnerManager).AutoInclude();

                entity.HasOne(p => p.PartnerManager)
                    .WithMany();
                entity.HasOne(p => p.Preference)
                    .WithMany();
            });

            base.OnModelCreating(modelBuilder);
        }
    }
}
